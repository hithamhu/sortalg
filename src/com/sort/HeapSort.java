/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sort;

/**
 *
 * @author Dream Tech
 */
public class HeapSort {
    
    //مجموع العناصر في المصفوفة التي تغيرت
    static int total;
    
    //this method convert root with child to know which value is bigest
    //arr this is array 
    //parentIndex is the root 
    static void heapfiy(int[] arr,int parentIndex){
        int leftNode=parentIndex*2;
        int rightNode=parentIndex*2+1;
        //اذا تم مقارنت الأب مع الأبناء وتبين ان هناك ابن اكبر من الأب يصبح هناك تغيير لقيمة الاب
        int newIndex=parentIndex; //the value should change 
        
        //change root or parent with left child
        if(leftNode<total && arr[leftNode]>arr[parentIndex])
            newIndex=leftNode;
        
         //change root or parent with right child // right must always be bigest of root
        if(rightNode<total && arr[rightNode]<arr[parentIndex])
            newIndex=rightNode;
        
        //if newIndex change this mean must make swap to array element
        if(newIndex!=parentIndex){
            swap(arr,parentIndex,newIndex);
            heapfiy(arr, newIndex);
        }
        
        
        
      
        
    }

    private static void swap(int[] arr, int parentIndex, int newIndex) {
        int temp=arr[parentIndex];
        arr[parentIndex]=arr[newIndex];
        arr[newIndex]=temp;
        
    }

    public HeapSort() {
         int[] heapArray={1,50,30,10,60,80};
        System.out.println("Befor heap sort");
        for(int i=0;i<heapArray.length;i++){
            System.out.print(heapArray[i]+ "\t");
        }
        
        System.out.println("\n after heap sort");
        mSort(heapArray);
        for(int i=0;i<heapArray.length;i++){
            System.out.print(heapArray[i]+ "\t");
        }
    }
    
    static void mSort(int[] arr){
        total=arr.length-1;
        // بناء الكومة إعادة ترتيب المصفوفة
        for(int i=total/2;i>=0;i-- ){
            heapfiy(arr, i);//عمل ترتيب للشجرة
        }
        
        for(int n=total;n>=0;n--){
            //// تحريك الجذر الحالي إلى النهاية
            swap(arr, 0, n);
            total--;
            heapfiy(arr, 0);
            
        }
        
               
    }
    
    
    
    
    
    
    
}
