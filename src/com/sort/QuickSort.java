/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sort;

/**
 *
 * @author Dream Tech
 */
public class QuickSort {
    
    
    static void QuickSort(int[] arr,int low,int high){
        
        //to stop this code use below if statement
        if(low>=high)
            return;
        //find the middle to be pivot
        int middle=low+(high-low)/2;
        int pivot=arr[middle];
        int i=low;
        int j=high;
        
        while(i<=j){
            while(arr[i]<pivot){
                i++;
            }
            while(arr[j]>pivot){
                j--;
            }
            if(i<=j){
                //here make swap betwen array element
                int temp=arr[i];
                arr[i]=arr[j];
                arr[j]=temp;
                i++;
                j--;
            }
        }
        if(low<j){
            //this divide array befor pivot
            QuickSort(arr, low, j);
        }
        
        if(high>i){
             //this divide array after pivot
            QuickSort(arr, i,high);
        }
        
    }
       public static void main(String[] args) {
         int[] mergeArray={1,8,7,4,3,2,5,9};
        System.out.println("Befor sort");
        for(int i=0;i<mergeArray.length;i++){
            System.out.print(mergeArray[i]+ "\t");
        }
           QuickSort(mergeArray, 0, mergeArray.length-1);
           
            System.out.println("\n after sort");
        for(int i=0;i<mergeArray.length;i++){
            System.out.print(mergeArray[i]+ "\t");
        }
       }
    
}
